Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cargo-edit
Upstream-Contact:
 Without Boats <lee@libertad.ucsd.edu>
 Pascal Hertleif <killercup@gmail.com>
 Sebastian Garrido <sebasgarcep@gmail.com>
 Jonas Platte <mail@jonasplatte.de>
 Benjamin Gill <git@bgill.eu>
 Andronik Ordian <write@reusable.software>
Source: https://github.com/killercup/cargo-edit

Files: *
Copyright:
 2015-2020 Without Boats <lee@libertad.ucsd.edu>
 2015-2020 Pascal Hertleif <killercup@gmail.com>
 2015-2020 Sebastian Garrido <sebasgarcep@gmail.com>
 2015-2020 Jonas Platte <mail@jonasplatte.de>
 2015-2020 Benjamin Gill <git@bgill.eu>
 2015-2020 Andronik Ordian <write@reusable.software>
License: Apache-2.0 or MIT

Files: debian/*
Copyright:
 2019-2020 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2019-2020 Robin Krahl <robin.krahl@ireas.org>
License: Apache-2.0 or MIT

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
